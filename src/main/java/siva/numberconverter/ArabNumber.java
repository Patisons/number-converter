package siva.numberconverter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

class ArabNumber {
    private final int value;
    private final List<Digit> digits = new ArrayList<>();

    ArabNumber(int value) {

        digits.add(new Digit(1000, "M"));
        digits.add(new Digit(900, "CM"));
        digits.add(new Digit(500, "D"));
        digits.add(new Digit(400, "CD"));
        digits.add(new Digit(100, "C"));
        digits.add(new Digit(90, "XC"));
        digits.add(new Digit(50, "L"));
        digits.add(new Digit(40, "XL"));
        digits.add(new Digit(10, "X"));
        digits.add(new Digit(9, "IX"));
        digits.add(new Digit(5, "V"));
        digits.add(new Digit(4, "IV"));
        digits.add(new Digit(1, "I"));

        this.value = value;
    }

    String toRoman() {
        StringBuilder builder = new StringBuilder();

        int remaining = value;

        for(Digit digit: digits) {
            while (remaining >= digit.arab) {
                remaining -= digit.arab;
                builder.append(digit.roman);
            }
        }
        return builder.toString();
    }
}
