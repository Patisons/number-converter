package siva.numberconverter;

public class RomanNumber {
    private String value;

    RomanNumber(String value) {

        this.value = value;
    }

    int toArab() {
        int number = 0;
        boolean existTwo = false;
        StringBuilder builder = new StringBuilder(this.value);
        DigitFind find = new DigitFind(builder, number, 1);

        while (builder.length() > 0) {
            if (builder.length() > 1) {
                find = new DigitFind(builder, number, 2);
                existTwo = find.digitFind();
            }
            if (existTwo) {
                builder = find.roman;
                number = find.number;
            } else {
                find = new DigitFind(builder, number, 1);
                find.digitFind();
                builder = find.roman;
                number = find.number;
            }
            existTwo = false;
        }
        return number;
    }
}
