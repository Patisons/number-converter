package siva.numberconverter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.lang.System.exit;

@SpringBootApplication
public class NumberConverterApplication {

    public static void main(String[] args) {

//		SpringApplication.run(NumberConverterApplication.class, args);
        new ConvertUserInput();
    }
}