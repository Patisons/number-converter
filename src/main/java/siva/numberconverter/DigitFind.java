package siva.numberconverter;

import java.util.ArrayList;
import java.util.List;

public class DigitFind {
    StringBuilder roman;
    int number, modes;
    private final List<Digit> Digits = new ArrayList<>();

    DigitFind(StringBuilder romanl, int numberl, int mode) {
        roman = romanl;
        number = numberl;
        modes = mode;
        if (modes == 1) {
            Digits.add(new Digit(1000, "M"));
            Digits.add(new Digit(500, "D"));
            Digits.add(new Digit(100, "C"));
            Digits.add(new Digit(50, "L"));
            Digits.add(new Digit(10, "X"));
            Digits.add(new Digit(5, "V"));
            Digits.add(new Digit(1, "I"));
        } else {

            Digits.add(new Digit(900, "CM"));
            Digits.add(new Digit(400, "CD"));
            Digits.add(new Digit(90, "XC"));
            Digits.add(new Digit(40, "XL"));
            Digits.add(new Digit(9, "IX"));
            Digits.add(new Digit(4, "IV"));
        }
    }

    boolean digitFind() {
        boolean yes = false;
        String tmp;
        tmp = roman.substring(0, modes);
        for (Digit digit : Digits) {
            if (tmp.equals(digit.roman)) {
                this.number += digit.arab;
                this.roman.delete(0, modes);
                yes = true;
                break;
            }
        }
        return yes;
    }
}
