package siva.numberconverter;

import java.util.Scanner;

public class ConvertUserInput {
    ConvertUserInput() {
        int mode;
        do {
            mode = menu();
            switch (mode) {
                case 1:
                    int number;
                    System.out.print("Ievadi arābu skaitli >");
                    number = inputInteger("Ievadi arābu skaitli >");
                    ArabNumber arabNumber = new ArabNumber(number);
                    System.out.print("Romieši skaitlis ir " + arabNumber.toRoman() + "\n");
                    break;
                case 2:
                    System.out.print("Ievadi romiešu skaitli >");
                    String stringNumber = inputString("Ievadi romiešu skaitli >");
                    RomanNumber romanNumber = new RomanNumber(stringNumber);
                    System.out.print("Arābu skaitlis ir " + romanNumber.toArab() + "\n");
                    break;
            }
        } while (!(mode == 0));
    }

    static int menu() {
        String izv;
        Scanner kin = new Scanner(System.in);
        int answer = 0, answerCount = 2;
        do {
            System.out.println("\n1 - No arābu uz romiešu");
            System.out.println("2 - No romiešu uz arābu");
            System.out.println("0 - Iziet");
            System.out.print("Izdari izveli >");
            izv = kin.nextLine();
            try {
                answer = Integer.parseInt(izv);
            } catch (NumberFormatException e) {
                new InputError();
                answer = -1;
            }
        } while (answer < 0 && answer > answerCount);
        return answer;
    }

    static int inputInteger(String notice) {
        String prelude;
        Scanner scaner = new Scanner(System.in);
        int intNumber = 0;
        do {
            prelude = scaner.nextLine();
            try {
                intNumber = Integer.parseInt(prelude);
            } catch (NumberFormatException e) {
                new InputError(notice);
                intNumber = -1;
            }
        } while (intNumber < 0);
        return intNumber;
    }

    static String inputString(String notice) {
        String prelude;
        Scanner kin = new Scanner(System.in);
        do {
            prelude = kin.nextLine();
            if (!prelude.matches("[IVXLCDM]{1,}")) {
                new InputError(notice);
                prelude = "";
            }
        }while (prelude.equals(""));
        return prelude;
    }
}
