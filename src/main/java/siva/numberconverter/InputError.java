package siva.numberconverter;

public class InputError {
    final String MESAGE = "Nekorekts ievads!";
    static String notice= "-----------------\n";

    InputError() {
        System.out.println(MESAGE);
        System.out.print(notice);
    }

    InputError(String notice) {
        System.out.println(MESAGE);
        System.out.print(notice);
    }
}
