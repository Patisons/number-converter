package siva.numberconverter;

class Digit {
    final int arab;
    final String roman;

    Digit(int arab, String roman) {
        this.arab = arab;
        this.roman = roman;
    }
}
