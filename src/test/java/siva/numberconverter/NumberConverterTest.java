package siva.numberconverter;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class NumberConverterTest {
//    arab test
    @Test
    public void convertOneFromRomanToArab() {
        RomanNumber number = new RomanNumber("I");
        assertEquals(1, number.toArab());
    }

    @Test
    public void convertTwoFromRomanToArab() {
        RomanNumber number = new RomanNumber("II");
        assertEquals(2, number.toArab());
    }
    @Test
    public void convertThreeFromRomanToArab() {
        RomanNumber number = new RomanNumber("III");
        assertEquals(3, number.toArab());
    }
    @Test
    public void convertFourFromRomanToArab() {
        RomanNumber number = new RomanNumber("IV");
        assertEquals(4, number.toArab());
    }
    @Test
    public void convertFiveFromRomanToArab() {
        RomanNumber number = new RomanNumber("V");
        assertEquals(5, number.toArab());
    }
    @Test
    public void convertSixFromRomanToArab() {
        RomanNumber number = new RomanNumber("VI");
        assertEquals(6, number.toArab());
    }
    @Test
    public void convertNineFromRomanToArab() {
        RomanNumber number = new RomanNumber("IX");
        assertEquals(9, number.toArab());
    }
    @Test
    public void convertFourteenFromRomanToArab() {
        RomanNumber number = new RomanNumber("XIV");
        assertEquals(14, number.toArab());
    }
    @Test
    public void convertNineteenFromRomanToArab() {
        RomanNumber number = new RomanNumber("XIX");
        assertEquals(19, number.toArab());
    }
    @Test
    public void convertLarge1FromRomanToArab() {
        RomanNumber number = new RomanNumber("XLVII");
        assertEquals(47, number.toArab());
    }
    @Test
    public void convertLarge2FromRomanToArab() {
        RomanNumber number = new RomanNumber("XCV");
        assertEquals(95, number.toArab());
    }
    @Test
    public void convertLarge3FromRomanToArab() {
        RomanNumber number = new RomanNumber("CDLII");
        assertEquals(452, number.toArab());
    }
    @Test
    public void convertLarge4FromRomanToArab() {
        RomanNumber number = new RomanNumber("MCDXLIX");
        assertEquals(1449, number.toArab());
    }
    @Test
    public void convertLarge5FromRomanToArab() {
        RomanNumber number = new RomanNumber("MMMCMXCIX");
        assertEquals(3999, number.toArab());
    }
//    roman test
    @Test
    public void convertOneFromArabToRoman() {
        ArabNumber number = new ArabNumber(1);
        assertEquals("I", number.toRoman());
    }

    @Test
    public void convertTwoFromArabToRoman() {
        ArabNumber number = new ArabNumber(2);
        assertEquals("II", number.toRoman());
    }

    @Test
    public void convertThreeFromArabToRoman() {
        ArabNumber number = new ArabNumber(3);
        assertEquals("III", number.toRoman());
    }


    @Test
    public void convertFourFromArabToRoman() {
        ArabNumber number = new ArabNumber(4);
        assertEquals("IV", number.toRoman());
    }

    @Test
    public void convertFiveFromArabToRoman() {
        ArabNumber number = new ArabNumber(5);
        assertEquals("V", number.toRoman());
    }

    @Test
    public void convertSixFromArabToRoman() {
        ArabNumber number = new ArabNumber(6);
        assertEquals("VI", number.toRoman());
    }

    @Test
    public void convertNineFromArabToRoman() {
        ArabNumber number = new ArabNumber(9);
        assertEquals("IX", number.toRoman());
    }

    @Test
    public void convertFourteenFromArabToRoman() {
        ArabNumber number = new ArabNumber(14);
        assertEquals("XIV", number.toRoman());
    }

    @Test
    public void convertNineteenFromArabToRoman() {
        ArabNumber number = new ArabNumber(19);
        assertEquals("XIX", number.toRoman());
    }


    @Test
    public void convertLargeComplexNumber() {
        ArabNumber number = new ArabNumber(27);
        assertEquals("XXVII", number.toRoman());
    }

    @Test
    public void convertLarge1ComplexNumber() {
        ArabNumber number = new ArabNumber(1449);
        assertEquals("MCDXLIX", number.toRoman());
    }
    @Test
    public void convertLarge2ComplexNumber() {
        ArabNumber number = new ArabNumber(3999);
        assertEquals("MMMCMXCIX", number.toRoman());
    }
}
